$(document).ready(function() {
    createToken();
    var noFoto = 1;
    var nameProgress = '';
    var images  = [];
    $("#file").on("change", function(){
        var token = $("#token").val();
        var archivos = document.getElementById('file').files;   
        var navegador = window.URL || window.webkitURL;
        for(x=0; x<archivos.length; x++){ 
            var objeto_url = navegador.createObjectURL(archivos[x]);
            var name_img = token+'_'+noFoto;
            $("#content_images").append('<div id='+name_img+' class="img-preview"><img src='+objeto_url+' /><div class="mascara d-flex m-0" onclick="deleteImage('+name_img+')"><i class="fas fa-times-circle text-white fa-4x justify-content-center align-self-center"></i></div></div>');
            noFoto++;
            images.push({
                "id" : name_img,
                "image" : archivos[x],
            });
        }
    });
    $("#form_images").submit(function(e){
        e.preventDefault();
        showUpload(images);
        $.post("images/create_action",{ nombre: $("#nombre").val(), password: $("#password").val(), ape_paterno: $("#ape_paterno").val(), ape_materno: $("#ape_materno").val(), email_personal: $("#email_personal").val(), email_alterno: $("#email_alterno").val(), fecha_nacimiento: $("#fecha_nacimiento").val(), fecha_festejo: $("#fecha_festejo").val(), token: $("#token").val() }, function(data, status){
            if(data==true){
                swal({
                    title: "\u00A1\u00C9xito!",
                    text: "Proceso realizado correctamente.",
                    type: "success"
                }).then(function(result) {
                    noFoto = 1;
                    $.post("mailing/send_token.php",{ email:$("#email_personal").val(), token:$("#token").val() }, function(data, status){ });
                    $("#content_images").html('');
                    $('#form_images').trigger("reset");
                    createToken();
                    images = [];
                });          
            }else{
                errorServer();   
            }
        });
    });
});

function showUpload(images){  
    var navegador = window.URL || window.webkitURL;
    for(x=0; x<images.length; x++){
        var objeto_url = navegador.createObjectURL(images[x]['image']);
        $("#content_upload").append('<div id="upload_'+images[x]['id']+'" class="col-1"><div class="img-upload"><img src='+objeto_url+' /></div><div class="progress p-0"><div id="progress_'+images[x]['id']+'" class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="width: 0%" aria-valuenow="0%" aria-valuemin="0" aria-valuemax="100"></div></div></div>');
    }
    uploadImage(images);
}
function uploadImage(images){  
    var xhr = new XMLHttpRequest();
    var formData = new FormData();
    for(x=0; x<images.length; x++){
        formData.append("images[]",images[x]['image']);
    }
        
    xhr.upload.addEventListener('progress', function(e) {
        for(x=0; x<images.length; x++){
            var percent = parseInt(e.loaded / e.total*100);
            document.getElementById("progress_"+images[x]['id']).style.width = percent+"%";
            if(percent == 100){
                $("#progress_"+images[x]['id']).delay(1000).queue(function(){
                    $(this).removeClass("bg-primary").addClass("bg-success").dequeue();
                });
                $("#upload_"+images[x]['id']).delay(5000).queue(function(){
                    $(this).addClass("filter").dequeue();
                    $(this).delay(5000).queue(function(){
                        $(this).remove();
                    });
                });
            }
        }
    }, true);
    
    formData.append("token",$("#token").val());
    xhr.open("POST","images/uploadImage");
    xhr.send(formData);
    xhr.abort;
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function errorServer() {
    swal({
        title: "\u00A1Error!",
        text: "Ha ocurrido un error en el servidor.",
        type: "error"
    });
}

function createToken() {
    $.post("images/generateRandomString",{ }, function(data, status){
        var token = $.trim(data);
        $("#token").val(token); 
    });
}

function deleteImage(name){
    $("#"+name.id).remove();
}