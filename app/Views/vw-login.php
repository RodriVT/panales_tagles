<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Pa&ntilde;ales Tagles</title> 
        <link rel="icon" href="assets/img/logo.png" type="image/ico">
        <link href="assets/css/styles.css" rel="stylesheet">
        <link href="assets/css/bootstrap-4.5.0.min.css" rel="stylesheet">
        <link href="assets/css/sweetalert-2.min.css" rel="stylesheet">
        <link href="assets/fonts/css/fontawesome.min.css" rel="stylesheet">
        <link href="assets/fonts/css/brands.min.css" rel="stylesheet">
        <link href="assets/fonts/css/solid.min.css" rel="stylesheet">
        <script src="assets/js/jquery-3.5.1.min.js"></script>
    </head>
    <div class="col-md-6 offset-md-3 h-100 d-none d-md-flex">
            <div class="justify-content-center align-self-center w-100">
                <div id="login" class="col-12 p-0 text-center">
                    <div id="logo_login">
                        <img src="assets/img/logo.png" alt=""/>
                    </div>
                    <div class="col-6 offset-2 input-design p-0 mt-4">
                        <div class="form-group">
                            <input type="text" class="form-control" id="username" name="username" placeholder="Usuario">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Contrase&ntilde;a">
                        </div>
                        <div class="col-12 text-center">
                            <button class="btn btn-app" onclick="login('web');">Acceder</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 p-0 h-100 d-md-none d-flex">
            <div class="justify-content-center align-self-center w-100">
                <form class="col-12 p-0 text-center" action={action_movil} method="post">
                    <div id="logo_login" class="mt-5">
                        <img src="assets/img/logo.png" alt=""/>
                    </div>
                    <div class="mt-3 mb-5 col-8 offset-2 input-design">
                        <div class="form-group">
                            <input type="text" class="form-control" id="username_movil" placeholder="Usuario">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password_movil" placeholder="Contrase&ntilde;a">
                        </div>
                        <div class="col-12 row p-0 m-0">
                             <div class="col-sm-6 offset-md-2 p-0">
                                <button class="btn btn-register" onclick="register();">Registrar</button>
                            </div>
                            <div class="col-sm-6 p-0">
                                <button class="btn btn-app" onclick="login('movil');">Acceder</button>
                            </div>   
                        </div>
                    </div>
                </form>
            </div> 
        </div>
    </body>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="assets/js/bootstrap-4.5.0.min.js"></script>
    <script src="assets/js/jquery.md5.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/sweetalert-2.min.js"></script>
    <script>
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
        var response = getParameterByName('response');
        if(response == "error"){
            swal({
                title: "\u00A1Alerta!",
                text: "El usuario/password son incorrectos.",
                type: "error"
            });
        }
        function register(){
            alert('registro');       
        }
        function login(data){
            var dispositivo = data;
            if(dispositivo == 'web'){
                alert('web');
            } else if(dispositivo == 'movil'){
                alert('movil');
            }
        }
    </script>
</html>