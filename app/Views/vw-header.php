<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Pics Upload Cloud</title>
        <link rel="icon" href="{level_page}assets/img/logo.png" type="image/ico" />

        <link href="{level_page}assets/css/styles.css" rel="stylesheet">
        <link href="{level_page}assets/css/bootstrap-4.5.0.min.css" rel="stylesheet">
        <link href="{level_page}assets/css/sweetalert-2.min.css" rel="stylesheet">
        <link href="{level_page}assets/fonts/css/fontawesome.min.css" rel="stylesheet">
        <link href="{level_page}assets/fonts/css/brands.min.css" rel="stylesheet">
        <link href="{level_page}assets/fonts/css/solid.min.css" rel="stylesheet">
        
        <script src="{level_page}assets/js/jquery-3.5.1.min.js"></script>
        <script src="assets/js/functions-generals.js"></script>
    </head>

    <body>
        <header id="header" class="d-fixed">
            <div class="col-12 p-0 d-flex">
               <div class="col-sm-2 col-lg-1 h4 text-primary p-3"><a onclick="openMenu()"><i class="fas fa-bars"></i></a></div>
                <div class="col-sm-10 col-lg-11 pt-2 d-flex">
                    <div class="icon-copy text-icon mr-3"><a href="#"><i class="fas fa-copy"></i></a></div>
                    <div class="icon-compressor text-icon mr-3"><a href="#"><i class="fas fa-file-archive"></i></a></div>
                    <div class="icon-upload text-icon mr-3"><a href="#"><i class="fas fa-cloud-upload-alt"></i></a></div>
                    <div class="icon-logout text-icon mr-3"><a href="#"><i class="fas fa-power-off"></i></a></div>
                </div>
            </div>
        </header>
        <div class="col-12 p-0 w-100 d-flex h-100">
            <div id="menu">
                <div class="list-group border-0 mt-5">
                    <a href="usuarios" class="list-group-item collapsed" data-parent="#sidebar"><i class="fas fa-user fa-lg mr-4"></i><span>Usuarios</span></a>
                    <a href="tokens" class="list-group-item collapsed" data-parent="#sidebar"><i class="fas fa-qrcode fa-lg mr-4"></i><span>C&oacute;digos</span>
                    <a href="images" class="list-group-item collapsed" data-parent="#sidebar"><i class="fas fa-cloud-upload-alt fa-lg ml-n1 mr-4"></i><span>Registrar Fotos</span></a></a>
                </div>
            </div>
            <div id="contenido" class="w-100">
                <div class="color-background col-12 p-3">
                    <div class="main col-12">
                    