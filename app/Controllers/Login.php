<?php namespace App\Controllers;

class Login extends BaseController
{
	public function index()
	{
        $parser = \Config\Services::parser();
        
        $data = [
            'action' => 'verifylogin/login',  
            'action_movil' => 'verifylogin/login_movil',  
        ];
        
        echo $parser->setData($data)->render('vw-login');
	}
}
